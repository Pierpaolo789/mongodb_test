<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
   
<div class="jumbotron text-center">
  <h1>Mongo Stress Test</h1>
  <p></p>
</div>

<div class="container">
  <form method="POST" action="{{ route('fakelogs.store') }}" >
    @csrf
    <div class="row">
      <div class="col-lg-6">
        <h3>N° Jobs to send</h3>
        <input type="number" id="quantity" name="quantity" min="1" max="999">
    
        <button type="submit" class="btn btn-primary"> 

        {{ __('Send') }}

        </button>
    </div>
    </form> 
    

    <form method="POST" action="{{ route('fakelogs.retrieve') }}" >
    @csrf
  
    <h3>N° Rows to read</h3>
      <input type="number" id="quant" name="quant" min="1" max="999">
  
       <button type="submit" class="btn btn-info"> 

        {{ __('Read') }}

       </button>
   
  
    </div>
    </form> 
    </div>

</html>
