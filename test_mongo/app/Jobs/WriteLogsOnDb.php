<?php

namespace App\Jobs;

use App\Models\LogMongo;
use App\Models\User;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;



class WriteLogsOnDb implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $params;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->params = [];
    }
    


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

      $this->params['uuid'] = bin2hex(random_bytes(979));
        $this->params['header'] = bin2hex(random_bytes(979));
       $this->params['request'] = bin2hex(random_bytes(979));
        $this->params['response'] = bin2hex(random_bytes(979));
        $this->params['http_status_code'] = bin2hex(random_bytes(979));
        $this->params['fase_id'] = bin2hex(random_bytes(979));
        $this->params['fase_description'] = bin2hex(random_bytes(979));
        $this->params['user_id'] = 123;
       $this->params['key_id'] = 123;    
       $this->params['AuthnRequest'] = bin2hex(random_bytes(979));
            
         $this->params['AuthnReq_ID'] = bin2hex(random_bytes(979));
          $this->params['AuthnReq_IssueInstant'] = bin2hex(random_bytes(979));
           $this->params['Resp_ID'] = bin2hex(random_bytes(979));
           $this->params['Resp_IssueInstant'] = bin2hex(random_bytes(979));
           $this->params['Resp_Issuer'] = bin2hex(random_bytes(979));
           $this->params['Assertion_ID'] = bin2hex(random_bytes(979));
          $this->params['Assertion_subject'] = bin2hex(random_bytes(979));
            $this->params['Assertion_subject_name_qualifier'] = bin2hex(random_bytes(979));
    
            try {

             //if(env('MONGO_LOG')){
                LogMongo::create([
                    'uuid' => bin2hex(random_bytes(979)),
                    'header' => bin2hex(random_bytes(979)),
                    'request' => bin2hex(random_bytes(979)),
                    'response' => bin2hex(random_bytes(979)),
                    'http_status_code' => bin2hex(random_bytes(979)),
                    'fase_id' => bin2hex(random_bytes(979)),
                    'fase_description' => bin2hex(random_bytes(979)),
                    'user_id' => 12333,
                    'key_id' => 12333,
                    'authn_request' => bin2hex(random_bytes(979)),
                    /*'authn_response' => $this->params['AuthnResponse'],*/
                    'authn_req_id' => bin2hex(random_bytes(979)),
                    'authn_req_issue_instant' => bin2hex(random_bytes(979)),
                    'resp_id' =>bin2hex(random_bytes(979)),
                    'resp_issue_instant' => bin2hex(random_bytes(979)),
                    'resp_issuer' =>bin2hex(random_bytes(979)),
                    'assertion_id' => bin2hex(random_bytes(979)),
                    'assertion_subject' => bin2hex(random_bytes(979)),
                    'assertion_subject_name_qualifier' => bin2hex(random_bytes(979))
                ]);
              // }

            }catch (Exception $exception) {
                //$this->saveLog('Exception creating log: ' .  $exception->getCode() . ' - ' .  $exception->getMessage() , 'info');
                $this->fail($exception);
            }
        }
        
    
}
