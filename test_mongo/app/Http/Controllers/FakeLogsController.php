<?php


namespace App\Http\Controllers;

use App\Models\LogMongo;
use App\Jobs\WriteLogsOnDb;
use Illuminate\Http\Request;

class FakeLogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function send(Request $request)
    {
       
       for($i=0; $i<$request->input('quantity'); $i++){
      
           dispatch(new WriteLogsOnDb());
           echo $i.'<br>';

       }

       dd($request);
        return view('books.create');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function retrieve(Request $request)
    {
       
        $ext_logs = LogMongo::where('user_id', 12333)->take((int)$request->input('quant'))->get()->toArray();

       dd($ext_logs);

        return view('books.create');
    }

   
}
