<?php


namespace App\Models;


use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Crypt;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class LogMongo extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'logs';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid',
        'header',
        'request',
        'response',
        'http_status_code',
        'fase_id',
        'fase_description',
        'user_id',
        'key_id',
        'authn_request',
        'authn_response',
        'authn_req_id',
        'authn_req_issue_instant',
        'resp_id',
        'resp_issue_instant',
        'resp_issuer',
        'assertion_id',
        'assertion_subject',
        'assertion_subject_name_qualifier'
    ];

  
}
